FROM public.ecr.aws/bitnami/python:3.8.12-prod
#python:3.8.12-slim
RUN echo '\
import os\n\
from http.server import HTTPServer, BaseHTTPRequestHandler\n\
class MyHandler(BaseHTTPRequestHandler):\n\
    def do_GET(self):\n\
        self.send_response(200)\n\
        self.end_headers()\n\
        env_name = os.environ.get("ENV_NAME")\n\
        print(f"returning {env_name}")\n\
        self.wfile.write(bytes(f"env is {env_name}", "utf-8"))\n\
httpd = HTTPServer(("", 3000), MyHandler)\n\
print("Hi there!")\n\
httpd.serve_forever()\n\
' >> app.py
EXPOSE 3000
ENTRYPOINT [ "python", "app.py" ]
